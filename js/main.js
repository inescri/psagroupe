$(document).ready(function() {

	$('#healthCategories').hide();
	$('#coachMod').hide();
	$('#actionsMod').hide();
	$('#cxMod').hide();
	$('#action-plan-window').hide();

	$('.categories-toggle').on('click', function() {
		$('#healthCategories').slideToggle();

			Highcharts.chart('categoriesChart', {
			    chart: {
			        type: 'column',
			        height: 280,
			    },
			    title: {
			        text: null
			    },
			    xAxis: {
			        categories: ['Category AVC', 'Category BVC', 'Category CVC', 'Category DVC', 'Category EVC', 'Category FVC', 'Category GVC', 'Category HVC']
			    },
			    credits: {
			        enabled: false
			    },
			    series: [{
			        name: 'My store',
			        color: '#555',
			        data: [91, 93, 94, 91, 92, 95, 92, 95]
			    }, {
			        name: 'Region',
			        data: [82, 72, 83, 82, 71, 74, 85, 84]
			    }, {
			        name: 'National',
			        color: '#435a6f',
			        data: [83, 84, 74, 72, 85, 75, 73, 85]
			    }]
			});
	});

	$('.c-main-health').click(function() {
	    $('.c-master-class').slideToggle( "slow", "linear" );
        $('#healthPillars').slideToggle( "slow", "linear" );

        $('.arrow-down img').toggleClass('rotate');
	});

	$('.coach-module-toggle').on("click", function() {
		$('.overall-health-row').slideToggle();
		$('#coachMod').slideToggle("slow", "linear");
	});

	$('.actions-module-toggle').on("click", function() {
		$('.overall-health-row').slideToggle();
		$('#actionsMod').slideToggle("slow", "linear");
	})

	$('.cx-module-toggle').on("click", function() {
		$('.overall-health-row').slideToggle();
		$('#cxMod').slideToggle("slow", "linear");
	})

	$('.create-action').on("click", function(){
		$('#action-plan-list').toggle("slide");
		$('#action-plan-window').toggle("slide");
	});

	$('.collapse').on('show.bs.collapse', function (e) {
        $('.collapse').not(this).removeClass('in');
    });

    $('.collapse').on('hide.bs.collapse', function (e) {
        $('.pillar').removeClass('small');
    });

    $('.pillar').on("click", function(){
    	$('.pillar').removeClass('small');
        $('.pillar').not(this).addClass('small');
    });
})