(function() {
	angular
		.module('zoop.reach')
		.component('botInsight', {
			templateUrl: 'app2/component/view/bot-insight.html',
			controller: 'BotInsightController as vm'
		})

	angular
		.module('zoop.reach')
		.controller('BotInsightController', BotInsightController)
		.directive('robot', robot)

	BotInsightController.$inject = ['$rootScope', '$scope', 'DataService', '$sce'];

	function BotInsightController($rootScope, $scope, DataService, $sce) {

		$scope.tour = [
			{content:'<h4>Welcome to REACH</h4> <span>My name is Sam and I will be your guide! </span> <p>REACH is short to "Raised Experience through Analysis, Commitment & Hummanity"</p>', 
			 index: 0},
			{content: '<p>Less is more, in REACH we have one metric! <br /> Your overall Health score</p>', index: 1},
			{content: '<p>This score is standing on 4 key pillars:</p> <ul><li>Compliance</li><li>Financial</li><li>Employee</li><li>Client</li></ul> <p>Click the arrow to get there!</p>', index: 2},
			{content: '<p>Well done! You must be wondering where we get these scores from?</p> <p>We sorted all the data points <strong>Customer satisfaction surveys, audit results, mystery leads, mystery visits</strong> into 4 pillars based on the question asked. For example: </p> <p>For <strong>Compliance</strong> we use questions such as:</p> <p><i>En arrivant dans le point de vente, y avait-il une signalétique permettant d\'identifier rapidement le service après-vente ?</i></p> <ol type="a"><li>Oui : décrivez</li><li>Non : décrivez</li><li>Non applicable</li></ol> <p>To measure your dealership compliance with the rules and regulations of PSA</p>', index: 3},
			{content: '<p>For <strong>Employee</strong> we use questions such as: </p> <p><i>Votre impression sur l\'accueil et la prise en charge du client.</i></p> <p>To measure your customers\' satisfaction with your services</p>', index: 4}
		]

		$scope.currentTip = $scope.tour[0];

		$scope.$watch('currentTip', function() {
			$scope.currentTipDisplay = $sce.trustAsHtml($scope.currentTip.content);	
		});

		$scope.nextTip = function(){
			var i = $scope.getTipIndex($scope.currentTip.index, 1);
			$scope.currentTip = $scope.tour[i];
		}

		$scope.previousTip = function(current){
			var i = $scope.getTipIndex($scope.currentTip.index, -1);
			$scope.currentTip = $scope.tour[i];
		}

		$scope.getTipIndex = function(currentIndex, shift){
			var len = $scope.tour.length;
			return (((currentIndex + shift) + len) % len)
		}

		$scope.endTour = function(){
			$(".c-bot-tour").hide();

			setTimeout(function () {
			    $(".c-bot-insight").show();
			}, 3000);
		}	

		$scope.insight = "Hello, I think you should install coffee machine and serve cookies.";

		$scope.markAs = function() {
		    $scope.insight = "Thanks for your feedback"
		};

		$('.view-here').hide();
		$scope.$on('submit', function(){
	        $scope.insight = "You have successfully created an action plan.";
	        $('.view-here').show();
	        $('.take-action-from-bot').hide();
      	})

      	$scope.$on('tipIndexes', function(){
      		$scope.currentTip = $scope.tour[3]
      	})

	}


	function robot() {
		return {
			link: link
		};

		function link(scope, elem, attr) {
		    $(".c-bot-insight, .c-bot-tour").hide();


			window.onload = function () {
			  	setTimeout(function () {
				    $(".c-bot-tour").show();
				}, 500);
			}

			$(".c-bot-button").click(function(){
				$(".c-bot-insight").toggle();
			});
		}
	}

})()