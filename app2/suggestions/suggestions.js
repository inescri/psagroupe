(function() {
	angular
		.module('zoop.reach')
		.component('reachSuggestions', {
			templateUrl: 'app2/suggestions/suggestions.html',
			controller: 'SuggestionsController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('SuggestionsController', SuggestionsController)

	SuggestionsController.$inject = ['$rootScope', '$scope'];

	function SuggestionsController($rootScope, $scope) {
		var vm = this;

		$scope.condition = 'existing';

		$scope.conditionItems = ["KPI", "##", "==", ">=", "<=", ">", "<", "AND", "OR", "NOT", "*", "/", "+", "-"];

	    $scope.clickToClone = function(event, cloneId) {
    		var sourceHtml = $(event.target).text();

    		if(sourceHtml.trim() == "KPI"){
    			angular.element(document.getElementById(cloneId)).append("<select class='condition-item'> <option>Test</option> </select>");
    		}else if(sourceHtml.trim() == "##"){
    			angular.element(document.getElementById(cloneId)).append("<input class='condition-item' placeholder='00'>");
    		}else{
    			angular.element(document.getElementById(cloneId)).append("<button class='condition-item'>" + sourceHtml + "</button>");
    		}
    	}
	}

})()