(function() {
	angular
		.module('zoop.reach', [
			'ngAnimate',
			'ui.router',
			'ui.bootstrap',
			'ngMap',
			'ngSanitize',
			'textAngular'
		]);
})()