(function() {
	angular
		.module('zoop.reach')
		.component('reachCxJourney', {
			templateUrl: 'app2/dashboard/view/cx-journey.html',
			controller: 'CxJourneyController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('CxJourneyController', CxJourneyController);

		CxJourneyController.$inject = ['$rootScope', '$scope', '$timeout', '$stateParams', 'DataService'];

        function CxJourneyController($rootScope, $scope, $timeout, $stateParams, DataService) {
            var vm = this;

            vm.steps = [];
            vm.params = $stateParams;

            vm.setTab = function(tab) {
                vm.tab = tab;
                stagger(vm.steps);
            }

            vm.$onInit = function() {

                vm.tab = 'newVehicle';
            	
            	var steps = {
                    newVehicle: [], afterSales: []
                };

            	DataService.request({
            		requestKey: 'cx-journey'
            	}).then(function(response) {
            		steps.newVehicle = response.data.newVehicle;
                    steps.afterSales = response.data.afterSales;
            	});

            	stagger(steps);
            }

            vm.isToggled = function(index) {
                return index == ($stateParams.step-1);
            }

            function stagger(steps) {

                $timeout(function() {
                    vm.steps.newVehicle = steps.newVehicle;
                    vm.steps.afterSales = steps.afterSales;
                }, 1000);
            }

        }
})()