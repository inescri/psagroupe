(function() {
    angular
        .module('zoop.reach')
        .directive('reachEditActionPlan', reachEditActionPlan);

        reachEditActionPlan.$inject = ['$window'];

        function reachEditActionPlan($window) {
            return {
                scope: {
                    step: '=',
                    index: '=',
                    toggled: '='
                },
                templateUrl: 'app2/dashboard/view/edit-action-plan.html',
                link: function(scope, elem, attrs) {

                    
                }
            }
        }
})()
