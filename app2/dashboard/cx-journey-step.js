(function() {
    angular
        .module('zoop.reach')
        .directive('reachCxJourneyStep', reachCxJourneyStep);

        reachCxJourneyStep.$inject = ['$window'];

        function reachCxJourneyStep($window) {
            return {
                scope: {
                    step: '=',
                    index: '=',
                    toggled: '='
                },
                templateUrl: 'app2/dashboard/view/cx-journey-step.html',
                link: function(scope, elem, attrs) {

                    if (scope.toggled) {
                       elem.find('.cx-step-popup').show();
                    } else {
                        elem.find('.cx-step-popup').hide();
                    }

                    console.log(scope.toggled)
                    
                    // elem.find('.cx-step-icon').click(function() {
                    //     scope.$broadcast('step-popup-opended');
                    //     elem.find('.cx-step-popup').toggle().addClass('animated fadeIn');
                    // })

                    // elem.find('.cx-step-popup-close').click(function() {
                    //     elem.find('.cx-step-popup').hide();
                    // });

                    // scope.$on('step-popup-opended', function() {
                    //     angular.element('.cx-step-popup').hide();
                    // });

                    elem.find('.cx-step-icon').click(function(){
                        elem.find('#cxJourneyInfo').modal();
                    })

                    angular.element($window).on('resize', function() {
                        flow();
                    });

                    flow();

                    function flow() {
                        var width = angular.element($window).width();
                        var height = angular.element($window).height();
                        var container = elem.find('.cx-step-popup .cx-index-items');

                        if (width <= 768 && height > 280) {
                            container.height(height- 180);
                        } else {
                            container.height(280);
                        }
                    }
                    
                }
            }
        }
})()
