(function() {
	angular
		.module('zoop.reach')
		.component('reachCoaching', {
			templateUrl: 'app2/dashboard/view/coaching.html',
			controller: 'CoachingController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('CoachingController', CoachingController);

	CoachingController.$inject = ['$rootScope', '$scope'];

	function CoachingController($rootScope, $scope) {
		var vm = this;
	}

})()