(function() {
	angular
		.module('zoop.reach')
		.component('reachVendors', {
			templateUrl: 'app2/dashboard/view/vendors.html',
			controller: 'VendorsController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('VendorsController', VendorsController)
		.directive('vendors', vendors);

	VendorsController.$inject = ['$rootScope', '$scope'];

	function VendorsController($rootScope, $scope) {
		var vm = this;

		$scope.vendors=[{
				"name": "Di Benedetto",
				"kpi": {
					"gloSat": 1,
					"recom": 1,
					"attitude": 1,
					"competence": 1,
					"delivery": 1,
					"accidents": 1
				}
			},{
				"name": "Van Sante",
				"kpi": {
					"gloSat": 2,
					"recom": 2,
					"attitude": 2,
					"competence": 2,
					"delivery": 2,
					"accidents":2 
				}
			},{
				"name": "Spranghers",
				"kpi": {
					"gloSat": 3,
					"recom": 3,
					"attitude": 3,
					"competence": 3,
					"delivery": 3,
					"accidents":3 
				}
			},{
				"name": "Delhaye",
				"kpi": {
					"gloSat": 4,
					"recom": 4,
					"attitude": 4,
					"competence": 4,
					"delivery": 4,
					"accidents":4 
				}
			},{
				"name": "Thomas",
				"kpi": {
					"gloSat": 5,
					"recom": 5,
					"attitude": 5,
					"competence": 5,
					"delivery": 5,
					"accidents":5 
				}
			}]

	}

	function vendors() {
		return {
			link: link
		};

		function link(scope, elem, attr) {


		    google.charts.load("visualization", "1", {'packages':['corechart']});
	      	google.charts.setOnLoadCallback(drawChart);

	      	var chart;

			function drawChart() {

	      		var data = new google.visualization.DataTable();
			    data.addColumn('string', 'Salesman');
			    data.addColumn('number', 'Score');
			    data.addColumn({type:'string', role:'style'});
			    data.addRows([
			        ['Delhaye', 	 42, '#1e2336'],
			        ['Van Sante',    23, '#5b3667'],
			        ['Spranghers',   20, '#932d6e'],
			        ['Di Benedetto', 10, '#e82555'],
			        ['Thomas',       05, '#83786f'],
			        ['Broeckaert',   20, '#d6d2c4'],
			        ['Roose', 	   	 50, '#4698cb'],
			        ['Gallopyn', 	 23, '#005587']
			    ]);

			    var options = {
		        	width: '100%',
	    			height: '100%',
		        	colors: ['#1e2336', '#5b3667', '#932d6e', '#e82555', '#83786f', '#d6d2c4', '#4698cb', '#005587'],
		        	backgroundColor: { fill: 'transparent'},
		        	legend: { position: 'none'},
		        	pieSliceText: 'label',
		        	pieHole: 0.4
			    };

			    pieChart = new google.visualization.PieChart(document.getElementById('pieChart'));
			    columnChart = new google.visualization.ColumnChart(document.getElementById('columnChart'));

			    pieChart.draw(data, options);
			    columnChart.draw(data, options);

			    var lastSelection = null;
		        
		        google.visualization.events.addListener(pieChart, 'ready', function(){
		          	if(lastSelection === null){
		            	lastSelection = 0;
		          	}else{
		            	changeSalesman(lastSelection);
		          	}
		        });

		        google.visualization.events.addListener(columnChart, 'ready', function(){
		          	if(lastSelection === null){
		            	lastSelection = 0;
		          	}else{
		            	changeSalesman(lastSelection);
		          	}
		        });
		        
		        
		        google.visualization.events.addListener(pieChart, 'select', function(){
		          var selectedItem = pieChart.getSelection()[0];
		          if (selectedItem) {
		          		options.slices = {[selectedItem.row]: {offset:'.1'}};
		            	changeSalesman(selectedItem.row);
		            	pieChart.draw(data, options);
		          }else{
		            	changeSalesman(lastSelection);
		          }

		        });
		        
		        google.visualization.events.addListener(columnChart, 'select', function(){
		          var selectedItem = columnChart.getSelection()[0];
		          if (selectedItem) {
		          		options.slices = {[selectedItem.row]: {offset:'.1'}};
		            	changeSalesman(selectedItem.row);
		            	pieChart.draw(data, options);
		          }else{
		            changeSalesman(lastSelection);
		          }
		        });

		        function changeSalesman(salesman){
		        	lastSelection = salesman;
		          	columnChart.setSelection([{row: salesman, column: 1}]);
		          	pieChart.setSelection([{row: salesman, column: null}]);

		          	var salesmanName = data.getValue(salesman, 0);
		          	var salesmanColor = data.getValue(salesman, 2);

		          	$('.vendor-name').text(salesmanName + ' (23 surveys)');
		          	$('.half-gauge').css('background', salesmanColor);
			    }

				$('.stats-box').click(function(){
					var kpi = $(this).find('h5').text();
					$('.kpi').text(kpi);

					var id = $(this).attr('id');
					switch(id){
						case 'gloSat':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 23, '#1e2336'],
								['Van Sante',    34, '#5b3667'],
								['Spranghers',   54, '#932d6e'],
						        ['Di Benedetto', 	 56, '#e82555'],
						        ['Thomas',       87, '#83786f'],
						        ['Broeckaert',   23, '#d6d2c4'],
						        ['Roose', 	   	 10, '#4698cb'],
						        ['Gallopyn', 	 29, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
						case 'recom':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 45, '#1e2336'],
								['Van Sante',    44, '#5b3667'],
								['Spranghers',   34, '#932d6e'],
						        ['Di Benedetto', 	 45, '#e82555'],
						        ['Thomas',       76, '#83786f'],
						        ['Broeckaert',   23, '#d6d2c4'],
						        ['Roose', 	   	 74, '#4698cb'],
						        ['Gallopyn', 	 54, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
						case 'attitude':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 53, '#1e2336'],
								['Van Sante',    12, '#5b3667'],
								['Spranghers',   54, '#932d6e'],
						        ['Di Benedetto', 	 23, '#e82555'],
						        ['Thomas',       45, '#83786f'],
						        ['Broeckaert',   67, '#d6d2c4'],
						        ['Roose', 	   	 78, '#4698cb'],
						        ['Gallopyn', 	 67, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
						case 'competence':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 73, '#1e2336'],
								['Van Sante',    65, '#5b3667'],
								['Spranghers',   23, '#932d6e'],
						        ['Di Benedetto', 	 45, '#e82555'],
						        ['Thomas',       57, '#83786f'],
						        ['Broeckaert',   78, '#d6d2c4'],
						        ['Roose', 	   	 54, '#4698cb'],
						        ['Gallopyn', 	 10, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
						case 'delivery':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 55, '#1e2336'],
								['Van Sante',    45, '#5b3667'],
								['Spranghers',   67, '#932d6e'],
						        ['Di Benedetto', 	 87, '#e82555'],
						        ['Thomas',       34, '#83786f'],
						        ['Broeckaert',   12, '#d6d2c4'],
						        ['Roose', 	   	 43, '#4698cb'],
						        ['Gallopyn', 	 12, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
						case 'accidents':
							data.removeRows(0,8);
							data.addRows([['Delhaye', 23, '#1e2336'],
								['Van Sante',    23, '#5b3667'],
								['Spranghers',   34, '#932d6e'],
						        ['Di Benedetto', 	 45, '#e82555'],
						        ['Thomas',       56, '#83786f'],
						        ['Broeckaert',   23, '#d6d2c4'],
						        ['Roose', 	   	 67, '#4698cb'],
						        ['Gallopyn', 	 90, '#005587']
							])

				      		columnChart.draw(data, options);
				      		pieChart.draw(data, options);
							break;
					 }
				});
	      	}


	      	$(window).resize(function(){
	      		drawChart();
	      	});


			$('.stats-box').click(function(){
				$('.stats-box').removeClass("active");
				$(this).addClass("active");
			});


		$('#statsBar').click(function(evt) {
			var elem = statsBar.getElementsAtEvent(evt);
			var name = elem[0];
			var vendorID = name._index + 1;
			$('.below-paragraphs').hide();
			$('.below-paragraphs.'+name._model.label).show();
			selectVendor(vendorID, name._model.label);
		});

		$('#statsPie').click(function(evt) {
			var elem = statsPie.getElementsAtEvent(evt);
			var name = elem[0];
			var vendorID = name._index + 1;

			selectVendor(vendorID, name._model.label);
		});
		
		function selectVendor(vendorID, vendorName) {
			$('.vendor-name').text(vendorName);
			//randomizing he values
			$('.stats-box h4').each(function(i, v) {
				var seed = Math.floor(Math.random()  * 78.2);
				if ($(this).hasClass('percent-field')) {
					seed = seed + '%';
				} 
				if ($(this).hasClass('day-field')) {
					seed = Math.floor(seed / 11) + 'd';
				} 
				
				$(this).text(seed);
			});
			
			//change all color to blue
			// $.each(statsGraph.data.datasets[0].backgroundColor, function(i, v) {
			// 	 statsGraph.data.datasets[0].backgroundColor[i] =  'rgba(28, 38, 47, 1)';
			// });

			var selectedVendor = statsBar.data.datasets[0].backgroundColor[vendorID - 1];
			$('.half-gauge').css('background', selectedVendor);
			
			//change one color to red$
			statsBar.data.datasets[0].borderColor[vendorID - 1] =  'rgba(173, 22, 22, 1)';
			statsBar.update();
		}
		}
	}

})()