(function() {
	angular
		.module('zoop.reach')
		.component('reachActionPlan', {
			templateUrl: 'app2/dashboard/view/action-plan.html',
			controller: 'ActionPlanController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('ActionPlanController', ActionPlanController);

	ActionPlanController.$inject = ['$rootScope', '$scope', 'DataService'];

	function ActionPlanController($rootScope, $scope, DataService) {
		var vm = this;

		vm.steps = {};

		vm.$onInit = function() {
        	DataService.request({
        		requestKey: 'cx-journey'
        	}).then(function(response) {
        		vm.steps['NEW VEHICLE'] = response.data.newVehicle;
                vm.steps['AFTER SALES'] = response.data.afterSales;
        	});
		}

		$scope.emptyMilestone = {
			"title": "",
			"startDate": "",
			"endDate": "",
			"items": [],
			"newItem": {
				"text": "",
				"assignee": "",
				"done": false
			}
		}

		$scope.activeActionPlan = {
			"id": "",
			"title": "",
			"status": "open",
			"dueDate": "",
			"comments": [{
				"author": "",
				"content": ""
			}],
			"milestones": [],
			"newMilestone": angular.copy($scope.emptyMilestone)
		};

		$scope.actionPlans = [{
			"id": 1,
			"title": "My first action plan",
			"status": "open",
			"dueDate": "08/18/2017",
			"comments": [{
				"author": "",
				"content": ""
			}],
			"milestones": [{
				"title": "My first milestone",
				"startDate": "",
				"endDate": "08/18/2017",
				"items": [{
					"text": "Action item 1",
					"assignee": "",
					"done": false
				},{
					"text": "Action item 2",
					"assignee": "",
					"done": false
				},{
					"text": "Action item 3",
					"assignee": "",
					"done": false
				}],
				"newItem": {
					"text": "",
					"assignee": "",
					"done": false
				}
			}],
			"newMilestone": angular.copy($scope.emptyMilestone)	
		},{
			"id": 2,
			"title": "My second action plan",
			"status": "open",
			"dueDate": "09/01/2017",
			"comments": [{
				"author": "",
				"content": ""
			}],
			"milestones": [{
				"title": "My first milestone",
				"startDate": "",
				"endDate": "09/01/2017",
				"items": [{
					"text": "Action item 1",
					"assignee": "",
					"done": false
				},{
					"text": "Action item 2",
					"assignee": "",
					"done": false
				}],
				"newItem": {
					"text": "",
					"assignee": "",
					"done": false
				}
			}],
			"newMilestone": angular.copy($scope.emptyMilestone)	
		}];

		$scope.emptyActionPlan = {
			"id": "",
			"title": "",
			"status": "",
			"dueDate": "",
			"comments": [{
				"author": "",
				"content": ""
			}],
			"milestones": [],
			"newMilestone": angular.copy($scope.emptyMilestone)
		};

		$scope.item = {
			add:function(items, item){
				var newItem = {
					text: item.text,
					assignee: 'me',
					done: false
				};
				items.push(newItem);
				item.text = '';
			},
			remove:function(items, item){
				items.splice(items.indexOf(item),1);
				console.log(items, item);
			},
			done:function(items, item){
				if(!item.done){
					item.done = true;
				}else{
					item.done = false;
				}
			}
		};

		$scope.milestone = {
			add:function(milestones, milestone){
				var newMilestone = angular.copy(milestone);
				milestones.push(newMilestone);
				$scope.activeActionPlan.newMilestone = angular.copy($scope.emptyMilestone);
			}
		};

		$scope.actionPlan = {
			add:function(action){
				var newAction = angular.copy(action);

				if(action.id == ""){					
					$scope.maxId = Math.max.apply(Math,$scope.actionPlans.map(function(action){return action.id;}));
					action.id = $scope.maxId + 1;
					$scope.actionPlans.push(action);
					$scope.activeActionPlan = angular.copy($scope.emptyActionPlan);
				}else{
					$scope.showForm = false;
				}
			}
		};

		$scope.action = {};
		$scope.action.inputs = [];

		$scope.milestones = [];
		$scope.newMilestone = [];

		$scope.addMilestone = function (newMilestone){
			$scope.milestones.push(newMilestone);
			$scope.newMilestone = [];
		}

		$scope.addActionItem = function () {
		    $scope.action.inputs.push({
		        item: $scope.action.newActionItem
		    });
		    $scope.action.newActionItem = '';
	    };
	    
	    $scope.removeActionItem = function (index) {
	      	$scope.action.inputs.splice(index, 1);
	    };

	   	$scope.getSuggestion = function (index){
	    	var suggestion = $(index.target).parent().find('p').text();
	    	$scope.action.inputs.push({
	    		item: (suggestion)
	    	})
	    	$(index.target).parent().remove();
	    }

	    $scope.openaddMilestoneModal = function(){
	    	$('#addMilestoneModal').modal();
	    }

	    $scope.createActionPlan = function(){
	    	$scope.showForm = true;
	    	$scope.activeActionPlan = angular.copy($scope.emptyActionPlan);
	    }

	    $scope.editActionPlan = function(id){
	    	for(var i in $scope.actionPlans){
	    		if($scope.actionPlans[i].id == id){
	    			$scope.activeActionPlan = $scope.actionPlans[i];
	    			console.log($scope.actionPlans[i]);
	    		}
	    	}
	    	$scope.showForm = true;
	    }

	}

})()