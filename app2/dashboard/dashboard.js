(function() {
	angular
		.module('zoop.reach')
		.component('reachDashboard', {
			templateUrl: 'app2/dashboard/view/dashboard.html',
			controller: 'DashboardController as vm',
			bindings: {}
		});

	angular
		.module('zoop.reach')
		.controller('DashboardController', DashboardController);

	DashboardController.$inject = ['$rootScope', '$scope', '$state', 'DataService'];

	function DashboardController($rootScope, $scope, $state, DataService) {

		var vm = this;
		vm.state = $state;

		vm.togglePillars 	= togglePillars;
		vm.pillarIsActive 	= pillarIsActive;
		vm.pillarToggle 	= pillarToggle;
		vm.pillarIsNotThis 	= pillarIsNotThis;
		vm.pillarModal		= pillarModal;

		vm.openCx 			= openCx;
		vm.openActionPlan	= openActionPlan;
		vm.openCoaching		= openCoaching;

		vm.$onInit = function() {

			vm.showPillars = false;
			vm.pillarActive = null;

			DataService.request({
				requestKey: 'health-pillars'
			}).then(function(response) {
				vm.healthPillars = response.data.pillars;
				vm.overallHealth = response.data.health;
			});

			$scope.$emit('dashboardInit');
			
		}

		function togglePillars() {
			vm.pillarActive = null;
			vm.showPillars = !vm.showPillars;

			$rootScope.$broadcast('tipIndexes', $scope.insight);
		}

		function pillarIsActive(pillar) {
			return pillar == vm.pillarActive;
		}

		function pillarToggle(pillar) {
			if (vm.pillarIsActive(pillar)) {
				vm.pillarActive = null;
			} else {
				vm.pillarActive = pillar;
			}
		}

		function pillarIsNotThis(pillar) {
			if (vm.pillarActive && !vm.pillarIsActive(pillar)) {
				return true;
			} else {
				return false;
			}
		}

		function pillarModal(){
			$('#pillarInfo').modal();
		}

		function openCx(){
			$rootScope.$broadcast('tipCx', $scope.insight);
		}

		function openActionPlan(){
			$rootScope.$broadcast('tipActionPlan', $scope.insight);
		}

		function openCoaching(){
			$rootScope.$broadcast('tipCoaching', $scope.insight);
		}
	}
})()