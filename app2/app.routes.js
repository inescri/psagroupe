(function () {

	angular
		.module('zoop.reach')
		.config(function($stateProvider, $urlRouterProvider) {

			$urlRouterProvider.otherwise('/dash');

			$stateProvider
				.state('dash', {
					url: '/dash',
					component: 'reachDashboard'
				})
				.state('dash.cx', {
					url: '/cx?step',
					component: 'reachCxJourney'
				})
				.state('dash.co', {
					url: '/co',
					component: 'reachCoaching'
				})
				.state('dash.ap', {
					url: '/ap',
					component: 'reachActionPlan'
				})
				.state('dash.v', {
					url: '/vendors',
					component: 'reachVendors'
				})
				.state('helpdesk', {
					url: '/help-desk',
					component: 'reachHelpDesk'
				})
				.state('suggestions', {
					url: '/suggestions',
					component: 'reachSuggestions'
				})
		})
})()
