(function() {
	angular
		.module('zoop.reach')
		.component('reachHelpDesk', {
			templateUrl: 'app2/helpdesk/helpdesk.html',
			controller: 'HelpDeskController as vm'
		});

	angular
		.module('zoop.reach')
		.controller('HelpDeskController', HelpDeskController)
		.directive('scrollTo', scrollTo);

	HelpDeskController.$inject = ['$rootScope', '$scope', '$location', '$anchorScroll'];

	function HelpDeskController($rootScope, $scope) {
		var vm = this;

		$scope.oneAtATime = true;

		$scope.status = {
		    isFirstOpen: true
		};
	}

    function scrollTo($location, $anchorScroll) {
		return function(scope, element, attrs) {
    		element.bind('click', function(event) {
				event.stopPropagation();
				scope.$on('$locationChangeStart', function(ev) {
				  ev.preventDefault();
				});
				var location = attrs.scrollTo;
				$location.hash(location);
				$anchorScroll();
			});
    	}
    }

})()